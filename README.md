# Solid vs Svelte

This is a turborepo containing packages to compare solid and svelte in terms of their runtime size

## What's inside?

This turborepo uses [pnpm](https://pnpm.io) as a package manager. It includes the following packages/apps:

### Apps and Packages

- `docs`: a [Next.js](https://nextjs.org/) app
- `web`: another [Next.js](https://nextjs.org/) app
- `ui`: a stub React component library shared by both `web` and `docs` applications
- `eslint-config-custom`: `eslint` configurations (includes `eslint-config-next` and `eslint-config-prettier`)
- `tsconfig`: `tsconfig.json`s used throughout the monorepo

Each package/app is 100% [TypeScript](https://www.typescriptlang.org/).

### Build

To build all apps and packages, run the following command:

```
cd solid-v-svelte
pnpm run build
```

### Build

To start all apps and packages, run the following command:

```
cd solid-v-svelte
pnpm run start
```

### Develop

To develop all apps and packages, run the following command:

```
cd solid-v-svelte
pnpm run dev
```
